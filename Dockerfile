FROM mcs07/rdkit:2020.03.2

# Install ubuntu packages
RUN apt update \
 && apt-get install -y git \
 && apt-get install -y unzip \
 && apt-get install -y  wget \
 && apt-get install -y  curl \
 && apt-get install -y  time \
 && apt-get install -y python3-pip

# Install python packages
RUN pip3 install --upgrade pip
RUN pip3 install cmake --upgrade
RUN pip3 install pandas numpy scikit-learn

# Download dependent files for scDrug
RUN git clone https://github.com/yysun0116/new_drug_prediction.git /scDrug


# Load shell script
COPY run.sh /app/
WORKDIR /app

CMD ["sh", "run.sh"]
